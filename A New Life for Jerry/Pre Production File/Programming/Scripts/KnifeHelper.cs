﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(HingeJoint))]
[AddComponentMenu("Knifes")]
[ExecuteInEditMode]
public class KnifeHelper : MonoBehaviour 
{
	//Only really used as a small little healper. only show the main varibles.

	[Header("Knife Settings")]
	[Tooltip("Use when tweeking setting in the editor. Turn off for optimization!")]
	public bool runInEveryFrame = true;

	[Header("What local axis does it pivot around?")]
	[Tooltip("Some objects up will be different.")]
	public Vector3 pivotAxis = Vector3.forward;

	[Header("How fast will it slow down?")]
	[Tooltip("Set 0 and will spin forever.")]
	public float deAcceleration = 3;

	HingeJoint joint;
	Rigidbody rigidBody;

	void Start () 
	{
		UpdateJointSettings ();
	}

	void Update () 
	{
		if (runInEveryFrame) 
		{
			UpdateJointSettings ();	
		}
	}

	public void UpdateJointSettings ()
	{
		joint = GetComponent<HingeJoint> ();
		rigidBody = GetComponent<Rigidbody> ();

		if (joint == null || rigidBody == null) 
		{
			Debug.LogWarning ("Knife Missing: Hinge Joint or Rigidbody");
			return;
		}

		rigidBody.angularDrag = deAcceleration;


		joint.axis = pivotAxis;

		joint.autoConfigureConnectedAnchor = true;

		//I have no idea what these do. but its work when its one.
		joint.breakForce = 1;
		joint.breakTorque = 1;
	}
}
