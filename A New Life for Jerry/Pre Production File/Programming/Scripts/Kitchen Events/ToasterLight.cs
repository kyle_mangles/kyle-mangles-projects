﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToasterLight : MonoBehaviour {

	// Use this for initialization
	public GameObject toastController;
	public GameObject toasterLight;
	public float lightTimer = 1.5f;
	public bool lightCheck = true;
	public bool isToastDown;


	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		isToastDown = toastController.GetComponent<ToasterButton> ().isPressed;

		lightTimer -= Time.deltaTime;
		if (isToastDown == false) 
		{
			if (lightTimer < 0) 
			{
				if (toasterLight.activeSelf == true) 
				{
					toasterLight.SetActive (false);
				} 
				else if (toasterLight.activeSelf == false) 
				{
					toasterLight.SetActive (true);
				}
				lightTimer = 1.5f;
			}
		} 

		else if (isToastDown == true) 
		{
			toasterLight.SetActive (true);
		}
	}
}
