﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class ToasterButton : MonoBehaviour {

	public PlayableDirector toasterButtonDirector;
	public bool isPressed = false;

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter (Collider other)
	{
		toasterButtonDirector.Play ();
		isPressed = true;
	}


}
