﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class ToasterController : MonoBehaviour {

	public PlayableDirector toasterUpDirector;
	public PlayableDirector toasterDownDirector;
	public bool buttonDown;
	public GameObject toasterController;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		buttonDown = toasterController.GetComponent<ToasterButton> ().isPressed;
	}

	void OnTriggerEnter (Collider other)
	{
		Debug.Log ("Trigger Fails");
		Debug.Log (buttonDown);
		if (buttonDown == true) 
		{
			Debug.Log ("Working" + buttonDown);
			toasterUpDirector.Play ();
			toasterController.GetComponent<ToasterButton>().isPressed = false;
			Debug.Log ("Controller Works");
		}

	}
}
