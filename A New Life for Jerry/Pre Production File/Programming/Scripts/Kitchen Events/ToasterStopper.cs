﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class ToasterStopper : MonoBehaviour {

	public PlayableDirector upDirector;
	public PlayableDirector downDirector;
	public PlayableDirector controllerDirector;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerStay (Collider other)
	{
		upDirector.Stop ();
		downDirector.Stop ();
		controllerDirector.Stop ();

		Debug.Log ("Directors Stopped");
	}
		
}
