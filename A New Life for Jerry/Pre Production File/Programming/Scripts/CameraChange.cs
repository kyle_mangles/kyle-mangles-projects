﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class CameraChange : MonoBehaviour {

	public PlayableDirector director;

	// Use this for initialization
	void Start () 
	{
	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") 
		{
			director.Play ();
			Debug.Log (other);
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Player") 
		{
			director.Stop ();
			Debug.Log (other);
		}
	}
}
