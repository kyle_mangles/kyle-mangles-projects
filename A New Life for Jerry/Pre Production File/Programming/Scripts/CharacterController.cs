﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    public float inputDelay = 0.5f;
    public float velocity = 5;
    public float acceleration = 5;
    public GameObject curCamera;
    public float torque;

    public bool isAlive = true;
    //public float rotateVel = 100;

    // Quaternion targetRotation;
    Rigidbody rBody;
    Vector3 norMovement;
    Vector3 movement;


    float curVelocity;
    float curAcceleration;

    float rampVelocity = 30;
    float rampAcceleration = 100;

    float radius;

    float tempVelocity, tempAcceleration;

    float moveHorizontal, moveVertical;



    // Use this for initialization
    void Start()
    {
        if (GetComponent<Rigidbody>())
            rBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("Character Needs A Rigidbody.");

        curVelocity = velocity;
        curAcceleration = acceleration;

        radius = GetComponent<SphereCollider>().radius + 1f;
    }

    void GetInput()
    {
        moveVertical = Input.GetAxis("Vertical");
        moveHorizontal = Input.GetAxis("Horizontal");
    }



    // Update is called once per frame
    void Update()
    {
        GetInput();

        RaycastHit doeshit = new RaycastHit();
        Physics.Raycast(transform.position, -Vector3.up, out doeshit, radius);

        rBody.useGravity = !(doeshit.collider.tag == "ramp");


        if (curAcceleration > 20)
        {
            Debug.Log("cunts fucked....");
        }
    }

    void FixedUpdate()
    {
        Roll();

    }


    void Roll()
    {
        if (Mathf.Abs(moveVertical) > (inputDelay) || Mathf.Abs(moveHorizontal) > (inputDelay))
        {
            //move
            Vector3 a = new Vector3(moveHorizontal, 0.0f, moveVertical);
            movement = curCamera.transform.TransformDirection(a);

            norMovement = movement.normalized * acceleration * Time.deltaTime;


            rBody.AddForce(norMovement * curVelocity);
        }
        else
        {
            Vector3 opposite = -rBody.velocity;
            float brakePower = 20;

            Vector3 brakeForce = opposite.normalized * brakePower;
            rBody.AddForce(brakeForce * Time.deltaTime);

        }
    }

    void PlayerDeath()
    {
        if (rBody.position.y <= 0.5f )
        {
            isAlive = false;
        }

        if (!isAlive)
        {
            GUILayout.BeginArea(new Rect(Screen.width / 2 - 300, Screen.height / 2 - 150, 600, 800));
            GUILayout.Label("Dead");

        }
    }


    //void OnCollisionEnter (Collision col)
    //{
    //    if (col.gameObject.tag == "ramp")
    //    {
    //        Debug.Log("Collider triggered! Fuck yis cunt!");

    //        rBody.useGravity = false;

    //        /*norMovement = movement.normalized * rampAcceleration * Time.deltaTime;
    //        rBody.AddForce(norMovement * rampVelocity);*/
    //    }
    //    else if (col.gameObject.tag != "ramp")
    //    {
    //        rBody.useGravity = true;
    //    }


   


      

}
